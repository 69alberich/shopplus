<?php namespace QchSoft\ShopPlus\Controllers;

use Backend\Classes\Controller;


class Inventory extends Controller{
    public $implement = [
        'Backend.Behaviors.ImportExportController',
    ];

    public $importExportConfig = 'config_import_export.yaml';

    
}
