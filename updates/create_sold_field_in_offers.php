<?php namespace Qchsoft\LocationExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class create_sold_field_in_offers extends Migration
{
    const TABLE_NAME = 'lovata_shopaholic_offers';
    
    public function up()
    {
        // Schema::create('qchsoft_yatchextension_table', function($table)
        // {
        // });
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'sold')) {
            return;
        }
        
         Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            $obTable->integer('sold')->default(0);
        });
    }

    public function down()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'sold')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            
            $obTable->dropColumn('sold');
    
        });
    }
}