<?php namespace Qchsoft\ShopPlus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftShopplusPaymentsStatus extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_shopplus_payments_status', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 150);
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_shopplus_payments_status');
    }
}
