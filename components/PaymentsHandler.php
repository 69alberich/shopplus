<?php namespace QchSoft\ShopPlus\Components;

use Cms\Classes\ComponentBase;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\Toolbox\Classes\Helper\PriceHelper;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use QchSoft\ShopPlus\Models\Payment;
use Lovata\OrdersShopaholic\Models\PaymentMethod;

use Input;
use Response;
use Mail;
use BackendAuth;
use System\Models\File;
use Lovata\Shopaholic\Models\Settings as ShopaHolicSettings;
use QchSoft\ShopPlus\Models\Settings as ShopPlusSettings;

class PaymentsHandler extends ComponentBase{
    
    protected $obUser;
    protected $obOrder;

    public function componentDetails()
    {
        return [
            'name'        => 'Payments Handler',
            'description' => 'test',
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title' => 'Code',
                'description' => 'Code used to find order',
                'type' => 'string',
                'required' => 'true',

            ],
            'type' => [
                "title" => 'Type',
                'description' => 'find by code or slug',
                'type' => "dropdown",
                'options'     => ['number'=>'by number', 'slug'=>'by slug']
            ]
        ];
    }
    public function init(){
        $user = UserHelper::instance()->getUser();
        if (!$user) {
           $user = BackendAuth::getUser();
        }
        $this->obUser = $user;
        //var_dump($this->obOrder);
        if ($this->property('type') == "slug") {
            $this->obOrder =  Order::getBySecretKey($this->property('code'))->first();
        }else{
            $this->obOrder =  Order::getByNumber($this->property('code'))->first();
        }
        
    }

    public function onAddPayment(){
        $data = post();
        $file = Input::file("capture");
       // trace_log(get_class($this->obUser));
       if (!isset($data["mount"])) {
        
        $nospace = str_replace(" ", "", $this->obOrder->total_price);
        $mount = str_replace(",", ".", $nospace);
       }else{
           $mount = $data["mount"];
       }
        
        $payment = new Payment();

        $payment->order_id = $this->obOrder->id;
        $payment->mount = $mount;
        $payment->reference = $data["reference"];
        
        if (isset($data["currency_id"])) {
            $payment->currency = $data["currency_id"];
        }else{
            $payment->currency = 1;
        }
        
        
        $payment->payment_status_id = $data["status_id"];
        $payment->usable_id = $this->obUser->id; //ECHARLE UN OJITO
        $payment->usable_type = get_class($this->obUser);
        $payment->file = $file;
        $payment->payment_method_id = $data["method_id"];
        if ($this->obOrder->status_id == 1) {
            $this->obOrder->status_id = $data["order_status_id"];
            $this->obOrder->payment_method_id = $data["method_id"];
        }

        
        if (isset($data["subsidiary"]) && $data["subsidiary"]!=null) {
            $this->obOrder->property["subsidiary"]= $data["subsidiary"];
        }
        $this->obOrder->save();

        if($payment->save()){
            
            //si en la configuración está activa descontar por esta accion

            
            $emailList = explode(",", ShopaHolicSettings::get("creating_order_manager_email_list"));
            $arOrder = array("order" => $this->obOrder->toArray());
            Mail::send("qchsoft.shopplus::mail.payment_received",
                $arOrder, function($message) use ($arOrder, $payment, $emailList) {
                foreach ($emailList as $email) {
                    $message->to($email);
                }
                
                if($payment->file){
                    $message->attach($payment->file->getPath());
                }
            });

            $paymentMethod = PaymentMethod::find($data["method_id"]);
           
            return Response::json(
            ['success' => true, 
             'payment_id' => $payment->id,
             'mount'=> $this->getConvertedMount($payment->mount),
             'currency' => $this->obOrder->currency->code,
             'shipping_type' => $this->obOrder->shipping_type->name,
             'payment_method' => $paymentMethod->name,
            ]);

        }else{
            return Response::json(['success' => false]);
        }
        
    }

    public function onPayInSite(){
        $data = post();
       
        $this->obOrder->status_id = 6;
        $this->obOrder->payment_method_id = 5;

        //$properties = $this->obOrder["properties"];
        $orderP = $this->obOrder->property;
        $orderP["subsidiary"] = $data["subsidiary"];

        $this->obOrder->addJsonable("property", $orderP);

        if($this->obOrder->save()){
            return Response::json(['success' => true]);
        }else{
            return Response::json(['success' => false]);
        }
    }

    public function getConvertedMount($mount){
        $defaultCurrency = CurrencyHelper::instance()->getDefault();
        $activeCurrencyCode = CurrencyHelper::instance()->getActiveCurrencyCode();
       //echo ($defaultCurrency);
       if($activeCurrencyCode == "USD"){
        $convertedPrice = $mount;
       }else{
        $convertedPrice = $mount/$defaultCurrency->rate;
       }
       return $convertedPrice;
    }

    public function getSecundaryMount($mount, $currencyCode){
        $defaultCurrency = CurrencyHelper::instance()->getDefault();
       
        if($currencyCode == "USD"){
           $convertedPrice = $mount*$defaultCurrency->rate;
       }else{
           $convertedPrice = $mount/$defaultCurrency->rate; 
       }
       
       return PriceHelper::format($convertedPrice) ;
    }

    public function getDefaultCurrency(){
        $defaultCurrency = CurrencyHelper::instance()->getDefault();
        return $defaultCurrency;
    }
}