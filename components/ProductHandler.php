<?php namespace QchSoft\ShopPlus\Components;

use Lovata\Shopaholic\Classes\Item\ProductItem;
use Lovata\Toolbox\Classes\Component\ElementData;

class ProductHandler extends ElementData
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'qchsoft.shopplus::lang.component.product_handler_name',
            'description' => 'qchsoft.shopplus::lang.component.product_handler_description',
        ];
    }

    public function onCheckProduct(){
        $data = post();
        $this->page['product_id_modal'] = $data["productId"];
        if(isset($data["prevQuantity"])){
            $this->page['prevQuantity'] = $data["prevQuantity"];
        }
        
        //trace_log($data);
        //return ['#someDiv' => $this->renderPartial('component-partial.htm')];
    }

    /**
     * Make new element item
     * @param int $iElementID
     * @return ProductItem
     */
    protected function makeItem($iElementID)
    {
        return ProductItem::make($iElementID);
    }
}