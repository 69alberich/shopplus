<?php namespace QchSoft\ShopPlus\Components;

use QchSoft\ShopPlus\Models\Settings;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class ShippingSettings extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Shipping Settings',
            'description' => 'Restrictions config from backend settings'
        ];
    }

    public function getAllowedDays(){
        return implode(",", Settings::get("days_of_week"));
    }

    public function getAllOptions(){
        $arrayOptions = array(
            'allowed_days' => $this->getAllowedDays()
        );

        return json_encode($arrayOptions);
    }
    public function getMaxShippingDate(){
        $maxDays = Settings::get("max_diff_day");
        $today = new Carbon();
        $maxDate = $today->addDays($maxDays);
        return $maxDate;
    }

    public function getTimeBlocks($mode=null){
        //OBTENGO LAS FECHAS DE LA CONFIGURACION EN STRING
        
        if($mode == "pickup"){

            $startDate = new Carbon(Settings::get("min_pick_up_time"));
            $endDate = new Carbon(Settings::get("max_pick_up_time"));

        }elseif($mode == "delivery" || $mode == null ){
            $startDate = new Carbon(Settings::get("min_time_shipping"));
            $endDate = new Carbon(Settings::get("max_time_shipping"));
        }
        
        $intervalBlockValue = Settings::get("time_block_interval");

        $pivotStartDate = new Carbon();
        $pivotEndDate = new Carbon();
        
        $pivotStartDate->timezone('America/Caracas');
        $pivotEndDate->timezone('America/Caracas');
        //SE TRANSFORMAN LAS FECHAS DE LA CONFIGURACION EN FORMATO CARBON
        $pivotStartDate->setTime($startDate->hour, $startDate->minute, $startDate->second);
        $pivotEndDate->setTime($endDate->hour, $endDate->minute, $endDate->second);
        
        $arrayDates = array();
        $arrayDates[0] = new Carbon($pivotStartDate);
        $i=1;

        while ($pivotEndDate >  $pivotStartDate ) {
            if($pivotStartDate->diffInHours($pivotEndDate) > 1 ){
                
                if ( ($pivotStartDate->hour + $intervalBlockValue)  <= 23) {
                    
                    $pivotStartDate->addHours($intervalBlockValue);
                    $arrayDates[$i] =  new Carbon($pivotStartDate); //array_push($arrayDates, $newStarDate);
                    
                }else{

                    $pivotStartDate->addHours(23 - $pivotStartDate->hour);
                    $arrayDates[$i] =  new Carbon($pivotStartDate); //array_push($arrayDates, $newStarDate);

                }
                               
                
            }else{
                $diff = $pivotStartDate->diffInMinutes($pivotEndDate);
                $pivotStartDate->addMinutes($diff);
                
                $arrayDates[$i] =  new Carbon($pivotStartDate); //array_push($arrayDates, $newStarDate);
            }
           
           $i++;
        }
        
        return $arrayDates;

    }

    public function getStartDate(){
        $allowShippingToday = Settings::get("allow_shipping_today");
        $now = new Carbon();
        $now->tz('America/Caracas');
        //si permite entregar el mismo dia	
        if ($allowShippingToday == 1) {
            
            $maxTimeShippingDate = new Carbon(Settings::get("max_time_shipping_today"));
            $pivotMaxTimeShippingDate = new Carbon();
            $pivotMaxTimeShippingDate->timezone('America/Caracas');
            
            $pivotMaxTimeShippingDate->setTime($maxTimeShippingDate->hour, $maxTimeShippingDate->minute, $maxTimeShippingDate->second);
            
            if ($now->diffInHours($pivotMaxTimeShippingDate) >= 1 ) {
               return $now;
            }else{
                return $now->addDay();
            }
        }else{
            return $now->addDay();
        }
    }

    public function getAllowedDates(){

        $startDate = $this->getStartDate();
        $endDate = $this->getMaxShippingDate();

        $arDates = array();

        $period = CarbonPeriod::create($startDate, '1 day', $endDate);

        foreach ($period as $key => $date) {
            array_push($arDates, $date);
        }

        return $arDates;

    }
}