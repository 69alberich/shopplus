<?php namespace QchSoft\ShopPlus\Components;

use Cms\Classes\ComponentBase;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use QchSoft\ShopPlus\Models\Payment;

class PaymentsList extends ComponentBase{

    protected $obUser;
    protected $obPaymentsList;
    protected $obOrder;

    public function componentDetails()
    {
        return [
            'name'        => 'Payments List',
            'description' => 'list of payments by user',
        ];
    }


    public function init(){
        $this->obUser = UserHelper::instance()->getUser();
        if($this->obUser){
            $this->obPaymentsList = Payment::getByUser($this->obUser->id)->get();
        }
        
        //
        //dump($this->obPaymentsList);
    }


    public function getPayments(){
        return $this->obPaymentsList;
    }

    public function getPaymentsByOrder($orderId){

        return Payment::getByOrderId($orderId)->get();
    }
    /* PRIMERO LLAMAR AL METODO  */
    public function getTotalPaymentsMount($orderId){
        $payments = Payment::getByOrderId($orderId)->get();
        $sum = 0;
        foreach ($payments as $payment) {
           $sum += $payment->mount;
        }
        return $sum;
    }   
}