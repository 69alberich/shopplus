<?php namespace QchSoft\ShopPlus\Components;

use BackendAuth;
use Cms\Classes\ComponentBase;

class ContactHandler extends ComponentBase {

    public function componentDetails()
    {
        return [
            'name' => 'ContactHandler',
            'description' => 'Methods for contact form'
        ];
    }

    public function defineProperties(){
        return [];
    }

    public function getPhoneCodes(){
        $service_url = 'https://restcountries.com/v2/all';
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            //die('error occured during curl exec. Additioanl info: ' . var_export($info));
            return array('+58' => '+58' );
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            //die('error occured: ' . $decoded->response->errormessage);
           return array('+58' => '+58' );
  
        }else{
          return $decoded;
        }
      }
}


