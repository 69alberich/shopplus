<?php namespace QchSoft\ShopPlus\Components;

use Cms\Classes\ComponentBase;
use Lovata\Toolbox\Classes\Helper\UserHelper;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use Lovata\Toolbox\Classes\Helper\PriceHelper;
use Lovata\OrdersShopaholic\Classes\Item\OrderPromoMechanismItem;
class OrdersHandler extends ComponentBase{

    protected $obUser;
    protected $obOrder;

    public function componentDetails()
    {
        return [
            'name'        => 'Orders Handler',
            'description' => 'Orders extra methods',
        ];
    }

    public function defineProperties(){
        return [
            'code' => [
                'title' => 'Code',
                'description' => 'Code used to find order',
                'type' => 'string',
                'required' => 'true',

            ],
            'type' => [
                "title" => 'Type',
                'description' => 'find by code or slug',
                'type' => "dropdown",
                'options'     => ['number'=>'by number', 'slug'=>'by slug']
            ]
        ];
    }

    public function init(){
        $this->obUser = UserHelper::instance()->getUser();
        //var_dump($this->obOrder);
        if ($this->property('type') == "slug") {
            $this->obOrder =  Order::getBySecretKey($this->property('code'))->first();
        }else{
            $this->obOrder =  Order::getByNumber($this->property('code'))->first();
        }
        
    }

    public function onChangeCurrency(){
        $data = post();
        $currency = CurrencyHelper::instance()->getDefault();
        /* SHIPPING PRICE */
        if ($this->obOrder->currency->code == "USD") {
            $convertedShippingPrice = PriceHelper::round(($this->obOrder->shipping_price_value * $currency->rate));
        }else{
            $convertedShippingPrice = PriceHelper::round(($this->obOrder->shipping_price_value / $currency->rate));
        }

        $this->obOrder->shipping_price = $convertedShippingPrice;
        /* ORDER POSITION */
        foreach ($this->obOrder->order_position as $obOderPositions) {

            if ($this->obOrder->currency->code == "USD") {
                $convertedTotalPrice = PriceHelper::round(($obOderPositions->price_value * $currency->rate));
                $convertedOldPrice = PriceHelper::round(($obOderPositions->old_price_value * $currency->rate));
            }else{
                $convertedTotalPrice = PriceHelper::round(($obOderPositions->price_value / $currency->rate));
                $convertedOldPrice = PriceHelper::round(($obOderPositions->old_price_value / $currency->rate));
            }
            $obOderPositions->price = $convertedTotalPrice;
            $obOderPositions->old_price = $convertedOldPrice;
            $obOderPositions->save();
         
        }
        /* ORDER PROMO */
        foreach ($this->obOrder->order_promo_mechanism as $obOrderPromo) {
            if ($this->obOrder->currency->code == "USD") {
                $convertedIncreasePrice = PriceHelper::round(($obOrderPromo->discount_value * $currency->rate));
            }else{
                $convertedIncreasePrice = PriceHelper::round(($obOrderPromo->discount_value / $currency->rate));
            }
            $obOrderPromo->discount_value = $convertedIncreasePrice;
            $obOrderPromo->save();

            OrderPromoMechanismItem::clearCache($obOrderPromo->id);
        }

        /* END*/
        if ($this->obOrder->currency->code =="USD") {
            $this->obOrder->currency_id = 2;
            
        }else{
            $this->obOrder->currency_id = 1;
            
        }

        $this->obOrder->save();
        
       
    }
}
