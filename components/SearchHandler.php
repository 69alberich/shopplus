<?php namespace QchSoft\ShopPlus\Components;

use Lovata\Toolbox\Classes\Component\SortingElementList;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;
use Lovata\Shopaholic\Classes\Collection\CategoryCollection;
use Lovata\Shopaholic\Classes\Item\PromoBlockItem ;
use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Classes\Store\ProductListStore;
use QchSoft\ShopPlus\Classes\Processor\SearchProductProcessor;
use Redirect;
use Input;
class SearchHandler extends SortingElementList
{
    
    protected $mainPage;
    private $searchParams;
    /**
     * @return array
     */
    public function componentDetails(){
        return [
            'name'        => 'qchsoft.shopplus::lang.component.search_handler_name',
            'description' => 'qchsoft.shopplus::lang.component.search_handler_description',
        ];
    }

    public function defineProperties(){
        return [
            'mode' => [
                'title' => 'Tipo de busqueda',
                'type' => 'dropdown',
                'default' => 'query'
            ],
            'search_param' => [
                'title' => 'Variable de busqueda',
                'type' => 'string',
                'placeholder' =>'opcional'
            ],
            'main_page'=>[
                'title' => 'Pagina principal a donde redirigir',
                'type' => 'string',
                
            ]
    
        ];
    }
    public function onRun(){
        //trace_log("estoy en run");
        $this->searchParams = get();

        if($this->property('search_param')!=""){
            
            $this->page["searchParams"] = $this->searchParams;
            
            $baseProductList = null;   
            if (isset($this->searchParams["category_id"]) && $this->searchParams["category_id"]!= 0) {

                $baseProductList = ProductCollection::make()->category($this->searchParams["category_id"],true)->active();

            }else{
                
                $baseProductList = ProductCollection::make()->active();
 
            }
            if (isset($this->searchParams["search"])) {
                $this->searchParams["search"] = str_replace("%", "", $this->searchParams["search"]);
                
                if ($baseProductList == null) {
                    $baseProductList = ProductCollection::make()->search($this->searchParams["search"])->active();
                }else{
                    $baseProductList->search($this->searchParams["search"])->active();
                }
            }
            if (isset($this->searchParams["subcategory"])) {
                $subcategories = explode("-", $this->searchParams["subcategory"]);
                
                if ($baseProductList == null) {
                    
                    $baseProductList = ProductCollection::make()->category($subcategories, true)->active();
                }else{
                    //trace_log($baseProductList);
                    $baseProductList->category($subcategories, true);
                }
            }

            if (isset($this->searchParams["sort"])) {
               $sortType =  str_replace("-", "|", $this->searchParams["sort"]);
               
               $baseProductList->sort($sortType);
            }


            $this->page['obProductList'] = $baseProductList;
        }
        $this->mainPage = $this->property('main_page');
        //$this->page['var'] = 'Maximum items allowed: ' . $this->property('main_page');
    }
    public function onSearch(){

        $data = Input::post();


        $data["page"]=1;

        if (isset($data["not_redirect"]) && $data["not_redirect"] == 0 ) {

            $data["search"] = str_replace("%", "", $data["search"]);

            $baseProductLists = ProductCollection::make()->search($data["search"])->active();
            if (isset($data["category_id"]) && $data["category_id"] != 0 ) {

                $baseProductLists->category($data["category_id"],true);
            }
            $this->page["suggestions"] = $baseProductLists;
            
        }elseif(!isset($data["not_redirect"]) || $data["not_redirect"] == 1){
            unset($data["not_redirect"]);
            return Redirect::to('busqueda/1/q?'.http_build_query($data));
        }
    
    }
    //no recuerdo si la uso XD
    public function onAutoComplete(){
        $data = post();
        
        //$arrayIds = SearchProductProcessor::search(trim($data["search"]), 10);
       // $baseProductLists = ProductCollection::make($arrayIds)->active();

       $baseProductLists = ProductCollection::make()->search(trim($data["search"]))->active();
        $arResult = array();
        foreach ($baseProductLists as $key => $obProduct) {
            $name =  preg_replace('/[^A-Za-z0-9\-]/', '', $obProduct->name);
            $arResult[$name] = null;
            
        }
        
        return $arResult;
    }

    public function onFilter(){

        $data = Input::post();

        $data["page"] = 1;
        $this->searchParams = $data;
        
        $baseProductList = null;
        

        if (isset($data["subcategory"]) && is_array($data["subcategory"])) {
            $data["subcategory"] = implode("-", $data["subcategory"]);
        }
        

        
        $this->page["searchParams"] = $this->searchParams;
        $this->page['obProductList'] = $baseProductLists = ProductCollection::make()->active();
        
        
        $this->page['page'] = 1;
        
        return Redirect::to('busqueda/1/q?'.http_build_query($data));
    }

    
    public function getSearchParamUrl(){
        if (isset($this->searchParams["page"])) {
            unset($this->searchParams["page"]); 
        }
        
        return http_build_query($this->searchParams);    
    }
    
    //INIT REFACT

    /**
     * Make element collection
     * @param array $arElementIDList
     *
     * @return ProductCollection
     */
    public function make($arElementIDList = null)
    {
        return ProductCollection::make($arElementIDList)->sort("no");
    }

    public function getBrandsArray($form){
        $brands = array();
        foreach ($form as $key => $value) {

            if ($this->startsWith($key,"brand")) {
                array_push($brands, $value);
            }
        }
        return $brands;
    }

    public function getCategoriesArray($form){
        $categories = array();
        foreach ($form as $key => $value) {

            if ($this->startsWith($key,"category")) {
                array_push($categories, $value);
            }
        }
        return $categories;
    }

    function startsWith ($string, $startString){ 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }

    function getProductsId($brandProduct){
        $array = array();

        foreach ($brandProduct as $key => $value) {
            //trace_log($value);
            if (!in_array($value->id, $array)) {
                array_push($array, $value->id);
            }
        }
        return $array;
    }

    public function getModeOptions(){
        return [
            'query'=>'Busqueda simple',
            'category'=>'Categoría',
            'brand' =>'Marca'
            ];
    }
}