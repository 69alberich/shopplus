<?php namespace Qchsoft\ShopPlus;

use System\Classes\PluginBase;
use Lovata\Shopaholic\Models\Settings;

use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use QchSoft\ShopPlus\Classes\Processor\InventoryProcessor;
use Lovata\Shopaholic\Models\Settings as ShopaHolicSettings;
use QchSoft\ShopPlus\Models\Settings as ShopPlusSettings;

use Lovata\Shopaholic\Controllers\Categories as CategoriesController;
use Lovata\Shopaholic\Models\Category as CategoryModel;
use Lovata\OrdersShopaholic\Models\Order as OrderModel;

use Lovata\Shopaholic\Classes\Collection\BrandCollection;
use Lovata\OrdersShopaholic\Classes\Collection\OrderCollection;
use Lovata\OrdersShopaholic\Classes\Item\OrderItem;

use Qchsoft\ShopPlus\Classes\Event\CategoryControllerHandler;
use Qchsoft\ShopPlus\Classes\Event\CategoryModelHandler;
use Qchsoft\ShopPlus\Classes\Event\OrdersControllerHandler;
use Qchsoft\ShopPlus\Classes\Event\OrderModelHandler;
use Qchsoft\ShopPlus\Classes\Event\ProductModelHandler;
use Qchsoft\ShopPlus\Classes\Event\ProductControllerHandler;

use Qchsoft\ShopPlus\Classes\Event\ExtendSettingsFieldHandler;

use Qchsoft\ShopPlus\Classes\Event\CategoryCollectionHandler;

use Qchsoft\ShopPlus\Classes\Event\ProductCollectionHandler;
use Qchsoft\ShopPlus\Classes\Event\BrandCollectionHandler;

use Qchsoft\ShopPlus\Classes\Event\OrderItemHandler;
use Qchsoft\ShopPlus\Classes\Event\OffersControllerHandler;

use Qchsoft\ShopPlus\Classes\Event\FeesHandler;

use QchSoft\ShopPlus\Classes\Restriction\RestrictionByCurrentDate;

use Lovata\OrdersShopaholic\Models\PromoMechanism;
use Lovata\OrdersShopaholic\Models\OrderPromoMechanism;

use Qchsoft\ShopPlus\Classes\Event\OfferModelHandler;

use Event;
use Carbon\Carbon;
use Crypt;
use Backend;
use BackendAuth;
use Mail;
use Db;

class Plugin extends PluginBase
{
    public function registerComponents(){
        return [
            'QchSoft\ShopPlus\Components\ProductHandler'   => 'ProductHandler',
            'QchSoft\ShopPlus\Components\SearchHandler'   => 'SearchHandler',
            'QchSoft\ShopPlus\Components\ShippingSettings'   => 'ShippingSettings',
            'QchSoft\ShopPlus\Components\PaymentsHandler'   => 'PaymentsHandler',
            'QchSoft\ShopPlus\Components\OrdersHandler'   => 'OrdersHandler',
            'QchSoft\ShopPlus\Components\PaymentsList'   => 'PaymentsList',
            'QchSoft\ShopPlus\Components\BackendUserHandler'   => 'BackendUserHandler',
            'QchSoft\ShopPlus\Components\ContactHandler'   => 'ContactHandler',
        ];
    }

    public function registerSettings(){
        return [
            'location' => [
                'label'       => 'Shopplus Settings',
                'description' => 'Manage custom settings',
                'category'    => 'Shopplus',
                'icon'        => 'icon-globe',
                'class'       => 'Qchsoft\Shopplus\Models\Settings',
                'order'       => 500,
                'keywords'    => 'settings opciones',
                
                'permissions' => ["manage-settings"]
                ]
        ];
    }

    public function registerMarkupTags(){
        return [
            'functions' => [
                'getFirstAvailableOffer' => function ($obProdct) {return $this->getFirstAvailableOffer($obProdct);},
                'getBrandsFromResult' => function($obProductList){return $this->getBrandsFromResult($obProductList);},
                'encrypt' => function($value){return $this->encrypt($value);}
            ],
        ];
    }

    public function registerSchedule($schedule)
    {
        
        $schedule->call(function () {
            
           $arOrders = OrderCollection::make()->status(1);
            
            //trace_log("por procesar:".count($arOrders));
            
            foreach ($arOrders as $obOrderItem) {

                if($obOrderItem->isAvailableToPay()){

                    $emailList = explode(",", Settings::get("creating_order_manager_email_list"));
                    
                    $arOrder = [
                        'order'        => $obOrderItem,
                        'order_number' => $obOrderItem->order_number,
                        'site_url'     => config('app.url'),
                    ];
                    $obOrder = $obOrderItem->getObject();
                   //trace_log();
                    Mail::send("lovata.ordersshopaholic::mail.create_order_user",
                        $arOrder, function($message) use ($arOrder, $obOrder) {
                            $message->to($obOrder->user->email);
                        
                    });
                    //trace_log("aqui");
                }else{
                    $order = $obOrderItem->getObject();
                    $order->status_id = 6;
                    
                    if (ShopPlusSettings::get("allow_restore_inventory")) {

                        //trace_log("repongo de orden".$order->id);

                        foreach ($order->order_position as $order_position) {
                            $order_position->offer->quantity += $order_position->quantity;
                            $order_position->offer->save();
                        }
                    }else{
                        trace_log("no habilidata la reposicion automatica");
                    }
                    

                    $order->save();

                    //trace_log("alla");
                }
                
           }
           // \Db::table('recent_users')->delete();
        })->hourly();
        
        $schedule->call(function () {
       
            $result =  InventoryProcessor::checkOfferQuantity();

            $emailList = explode(",", ShopaHolicSettings::get("creating_order_manager_email_list"));
            $form = array("form" => $result, "title" => "Productos activos agotados:");
            
            Mail::send("qchsoft.shopplus::mail.inventory-check",
            $form, function($message) use ($emailList) {
            foreach ($emailList as $email) {
                $message->to($email);
            }
            $message->subject("Productos agotados");
           
            
            });
            
        })->cron("0 */4 * * *");
        //})->everyMinute();
        
        $schedule->call(function () {
            //Productos activos agotados:
            $result =  InventoryProcessor::checkActiveProduct();

            $emailList = explode(",", ShopaHolicSettings::get("creating_order_manager_email_list"));
            $form = array("form" => $result, "title" => "Productos inactivos:");
            
            Mail::send("qchsoft.shopplus::mail.inventory-check",
            $form, function($message) use ($emailList) {
            foreach ($emailList as $email) {
                $message->to($email);
            }
            $message->subject("Productos Inactivos");
        
            
            });
            
        })->cron("0 */4 * * *");
        
        //})->everyMinute();
    }

    private  function getFirstAvailableOffer($obProduct){
        //trace_log($obProdct);
        $offers = $obProduct->offer;
        $firstOffer = null;
        foreach ($offers as $offer) {
            if($offer->price_value > 0 && $offer->quantity > 0 && $firstOffer == null){
                $firstOffer = $offer;
            }
        }
        return $firstOffer;
    }

    private function getBrandsFromResult($obProductList){
        $brandsIds = array();
        foreach ($obProductList as  $product) {
            $brandId = $product->brand->id;
            if ($brandId != null && !in_array($brandId, $brandsIds)) {
                array_push($brandsIds, $product->brand->id);
            }
        }
        return BrandCollection::make($brandsIds);
    }

    private function encrypt($value){
        $secret = Crypt::encrypt($value);
        return $secret;
    }

    

    public function boot(){
        
        \Lovata\Shopaholic\Classes\Store\ProductListStore::instance()->sorting->clear('sold|desc');

        $this->addEventListener();

        Event::listen('backend.menu.extendItems', function($manager) {
         
            $manager->addSideMenuItems('Lovata.Shopaholic', 'shopaholic-menu-main', [
                'shopaholic-menu-inventory' => [
                    'label' => 'Inventory',
                    'url' => Backend::url('lovata/shopaholic/offers/list'),
                    'order' => 400,
                    'icon' => 'icon-book'
                ]
            ]);

        });
          
        Event::listen('backend.list.extendQuery', function ($listWidget, $query) {
            //trace_log(get_class($listWidget->getController()));
            $userMaster = BackendAuth::getUser();
            if (get_class($listWidget->getController()) == "Lovata\OrdersShopaholic\Controllers\Orders") {
                if (!$userMaster->hasAccess("see-all-orders")) {
                  $query->where("manager_id", $userMaster->id);
                }
            }
           /* if (get_class($listWidget->getController()) !== 'Backend\Controllers\Users')
                return;
            $query->whereNull('department_id');
            $query->whereNull('organization_id');*/
        });

        Event::listen('shopaholic.sorting.get.list', function($sSorting) {

            if ($sSorting != 'sold|desc') {
                return null;
            }
            
            $arElementIDList = (array) DB::table('lovata_shopaholic_offers')
            ->select('lovata_shopaholic_offers.product_id')
            
            ->where('lovata_shopaholic_offers.active', true)
            ->orderBy('lovata_shopaholic_offers.sold', 'desc')
            ->lists('product_id');
            
            

            return $arElementIDList;
        });
    }

    protected function addEventListener(){

        Event::subscribe(CategoryModelHandler::class);
        Event::subscribe(OrderModelHandler::class);
        Event::subscribe(ProductModelHandler::class);
        Event::subscribe(ProductControllerHandler::class);
        Event::subscribe(ProductCollectionHandler::class);
        Event::subscribe(BrandCollectionHandler::class);
        Event::subscribe(CategoryControllerHandler::class);
        Event::subscribe(OrdersControllerHandler::class);
        Event::subscribe(ExtendSettingsFieldHandler::class);
        Event::subscribe(CategoryCollectionHandler::class);
        Event::subscribe(OrderItemHandler::class);
        Event::subscribe(OffersControllerHandler::class);
        Event::subscribe(FeesHandler::class);
        Event::subscribe(OfferModelHandler::class);
        
        /*Event::listen(\Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor::EVENT_UPDATE_ORDER_BEFORE_CREATE, function($arOrderData, $obUser) {
            
            $newUserData = array();
            $newUserData["property"]["document"] = $arOrderData["property"]["document_type"]."-".$arOrderData["property"]["document"];
            
            $obUser->password = null;
            $obUser->fill($newUserData);
            $obUser->save();

            return true;
        });*/

        


        Event::listen('shopaholic.order.created', function($obOrder) {
            
            InventoryProcessor::reduceInventoryByOrder($obOrder);
            $userMaster = BackendAuth::getUser();
            if ($userMaster!=null) {
               $obOrder->manager_id = $userMaster->id;
               $obOrder->save();
            }

        });
        Event::listen(\Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor::EVENT_UPDATE_ORDER_DATA, function($arOrderData) {
            //$arOrderData - array with order data from request
            //Change order data from request
            $defaultCurrency = CurrencyHelper::instance()->getDefault();
            $arOrderData["property"]["rate"] = $defaultCurrency->rate;
           
            return $arOrderData;
        });
        
        /*
        Event::listen(\Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor::EVENT_ORDER_USER_CREATED, function($obUser) {
            $obUser->password ="DCARNES2021";
            $obUser->password_confirmation ="DCARNES2021";

            $obUser->save();
            
        });*/
        
    
       Event::listen(\Lovata\OrdersShopaholic\Models\ShippingRestriction::EVENT_GET_SHIPPING_RESTRICTION_LIST, function() {
        $arResult = [
            RestrictionByCurrentDate::class => 'Custom Restriction by Total price',
        ];
            return $arResult;
        });

        

    }

}
