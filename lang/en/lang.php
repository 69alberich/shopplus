<?php return [
    'plugin' => [
        'name' => 'Shop plus',
        'description' => ''
    ],

    'component'   => [
        //ProductHandler Component
        'product_handler_name'            => 'Product Handler',
        'product_handler_description'     => 'Methods for use via ajax',

        'search_handler_name'            => 'Search Handler',
        'search_handler_description'     => 'Methods for search',
    ],
];