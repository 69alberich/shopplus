<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Product as Product;
use Lovata\Shopaholic\Models\Offer as Offer;
use Event;
use Lovata\Shopaholic\Classes\Import\ImportProductModelFromCSV;

class ProductModelHandler {

    public function subscribe(){

        Product::extend(function($model) {
            $model->addDynamicMethod('scopeBrands', function($query, $brandsIds = null) {
                return $query->whereIn('brand_id', $brandsIds);  
            });
        });

        Event::listen(ImportProductModelFromCSV::EVENT_AFTER_IMPORT, function ($obModel, $arImportData) {
            if (!$obModel instanceof Product) {
                return;
            }

            $arImportData["product_id"] = $obModel->id;
            
            if(isset($arImportData["price"])){
                $priceFormatted = str_replace(",", ".", $arImportData["price"]);
                $arImportData["price"] = $priceFormatted;
            }
            

            $offer = Offer::getByExternalID($arImportData["external_id"])->first();

            if ($offer != null ) {
            $offer->update($arImportData);
            }else{
                $offer = new Offer();
                $offer->fill($arImportData);
            }
            
            if (isset($arImportData["preview_image"])) {
                
                $previewImg = trim(array_get($arImportData, 'preview_image'));
                //trace_log($previewImg);

                if(file_exists($previewImg)){
                    $offer->preview_image = storage_path($previewImg);
                }
                
            }
            
            $offer->save();
                
            }, 900);

    }
}

/*
    Splice in configuration safely
            $myConfigPath = '$/qchsoft/shopplus/config/order_payment_relation.yaml';

            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );
 */