<?php namespace Qchsoft\ShopPlus\Classes\Event;



use Event;
use QchSoft\ShopPlus\Models\Settings as ShopPlusSettings;
use Carbon\Carbon;

use Lovata\OrdersShopaholic\Models\PromoMechanism;
use Lovata\OrdersShopaholic\Models\OrderPromoMechanism;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;

class FeesHandler {

    public function subscribe(){

        Event::listen(\Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor::EVENT_UPDATE_ORDER_BEFORE_CREATE, function($arOrderData, $obUser) {
            

            return true;
        });

        Event::listen(\Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor::EVENT_UPDATE_ORDER_AFTER_CREATE, function ($obOrder) {
            $nightFee = ShopPlusSettings::get("night_fee");
            
            $currentCurrency = CurrencyHelper::instance()->getActive();
            $defaultCurrency = CurrencyHelper::instance()->getDefault();

            if ($nightFee == true) {

                if ($this->isApplyAble($obOrder->getProperty("shipping_time")) == true ) {
                    $obMechanism = PromoMechanism::find(1);
                    
                    if ($currentCurrency->code == "USD") {
                        $convertedDiscountValue = $obMechanism->discount_value ;
                    }else{
                        $convertedDiscountValue = $defaultCurrency->rate * $obMechanism->discount_value;     
                    }
                    
                    try {
                        OrderPromoMechanism::create([
                            'order_id'       => $obOrder->id,
                            'mechanism_id'   => $obMechanism->id,
                            'name'           => $obMechanism->name,
                            'type'           => $obMechanism->type,
                            'priority'       => $obMechanism->priority,
                            'increase'      => 1,
                            'discount_value' => $convertedDiscountValue,
                            'discount_type'  => $obMechanism->discount_type,
                            'final_discount' => $obMechanism->final_discount,
                            'property'       => $obMechanism->property,
                            'element_data'   => [],
                        ]);
                    } catch (\Exception $obException) {
                        trace_log($obException);
                        return;
                    }
                }
            
            }   
        });
            
        Event::listen(\Lovata\OrdersShopaholic\Models\OrderPromoMechanism::EVENT_GET_DESCRIPTION, function ($obOrderMechanism) {
            /** @var OrderPromoMechanism $obOrderMechanism */
            $sResult = null;
            if ($obOrderMechanism->mechanism_id  == 1) {
                $sResult = "Suplemento nocturno";
            }
        
            return $sResult;
        });

    }


    public function isApplyAble($shippingTime){

        trace_log($shippingTime);

        $baseTime = explode("-", $shippingTime);

        $selectedTime = Carbon::createFromTimeString($baseTime[0]);

        //$selectedTime->setTimezone('America/Caracas');
        $selectedTime->setTimezone('UTC -4');

        trace_log($selectedTime->format("Y-m-d H:i:s"));
        
        $settingStartDate =  Carbon::createFromTimeString(ShopPlusSettings::get("start_nightfee_time"));
        $settingStartDate->setTimezone('UTC -4');

        trace_log("principio:".$settingStartDate->format("Y-m-d H:i:s"));

        $settingEndDate =  Carbon::createFromTimeString(ShopPlusSettings::get("end_nightfee_time"));
        $settingEndDate->setTimezone('UTC -4');

        trace_log("final:".$settingEndDate->format("Y-m-d H:i:s"));
        
        $intervalStartDate = Carbon::createFromTimeString($settingStartDate->format("H:i:s"));
        //$intervalStartDate->setTimezone('UTC -4');

        $intervalEndDate = Carbon::createFromTimeString($settingEndDate->format("H:i:s"));
        //$intervalEndDate->setTimezone('UTC -4');


        //trace_log("inicio:".$intervalStartDate->format("Y-m-d H:i:s")." y fin:".$intervalEndDate->format("Y-m-d H:i:s"));
        if ($selectedTime->between($intervalStartDate, $intervalEndDate)) {
            
            trace_log("no aplico el suplemento");
            return false;
        }else{
            trace_log("aplico el suplemento");
            return true;
        }
    }
}




