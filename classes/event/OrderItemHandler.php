<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\OrdersShopaholic\Classes\Item\OrderItem;
use Lovata\Shopaholic\Models\Settings;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use Lovata\Toolbox\Classes\Helper\PriceHelper;

use Carbon\Carbon;
use Mail;

class OrderItemHandler {


    public function subscribe(){
        OrderItem::extend(function($obItem) {
     
            $obItem->arExtendResult[] = "isAvailableToPay";
            $obItem->arExtendResult[] = "getExpirationDate";
            $obItem->arExtendResult[] = "$obItem";

            $obItem->addDynamicMethod('isAvailableToPay', function() use ($obItem) {
                //trace_log("entre aqui");
                $obModel = $obItem->getObject();
                //$obItem->setAttribute('isAvailableToPay', $obModel->created_at);
                $settings = Settings::instance();
                $quantity = $settings->orders_number;
                $format = $settings->orders_code;
                $created_date = $obModel->created_at->setTimezone("America/Caracas");
                $now = Carbon::now()->setTimezone("America/Caracas");
                

                //trace_log($created_date);

                if($format == 'd'){

                    $length = $created_date->diffInDays($now);
                    //$expiration_date->addDays($quantity);
                    

                }elseif($format == 'h'){

                    $length = $created_date->diffInHours($now);
                    //$expiration_date->addHours($quantity);
                    

                }elseif ($format == 'w') {

                    $length = $created_date->diffInWeeks($now);
                    //$expiration_date->addWeeks($quantity);

                }
                
                if ($length >=  $quantity) {
                    
                    return false;
                    
                }else{
                    return true;
                }
                
            });
             
            $obItem->addDynamicMethod('getExpirationDate', function() use ($obItem) {
                $settings = Settings::instance();
                $quantity = $settings->orders_number;
                $format = $settings->orders_code;

                $obModel = $obItem->getObject();

                $created_date = $obModel->created_at->setTimezone("America/Caracas");
                $now = Carbon::now()->setTimezone("America/Caracas");

                $expiration_date = $obModel->created_at;

                if($format == 'd'){

                    $length = $created_date->diffInDays($now);
                    $expiration_date->addDays($quantity);
                    

                }elseif($format == 'h'){

                    $length = $created_date->diffInHours($now);
                    $expiration_date->addHours($quantity);
                    

                }elseif ($format == 'w') {

                    $length = $created_date->diffInWeeks($now);
                    $expiration_date->addWeeks($quantity);

                }

                return $expiration_date;
            });

            $obItem->addDynamicMethod('getAlternativeCurrency', function() use ($obItem) {
                

                $obModel = $obItem->getObject();
                
                if ($obModel->currency->code == "USD") {
                
                    $currency = CurrencyHelper::instance()->getDefault();

                    $converted = PriceHelper::round(($obItem->total_price_value * $currency->rate)); 

                }else{
                    $converted = CurrencyHelper::instance()->convert($obItem->total_price_value, "USD");
                   
                }

                echo("tengo:".$converted);

            });

            
           
             

            
       });
    }

}
