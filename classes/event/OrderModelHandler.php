<?php namespace QchSoft\ShopPlus\Classes\Event;

use Lovata\Toolbox\Classes\Event\ModelHandler;
use Lovata\OrdersShopaholic\Models\Order as OrderModel;
use Lovata\Shopaholic\Models\Settings;
use Qchsoft\ShopPlus\Models\Payment;

class OrderModelHandler extends ModelHandler{

    public function subscribe($obEvent){
        
        
        OrderModel::extend(function($model) {

            if (!$model instanceof OrderModel) {
               
                return;
            }
            //pregunta si el modelo ha sido creado, si lo descomento no funciona en create 
            /*if (!$model->exists) {
              
                return;
            }*/
            //'detalles' => ['HesperiaPlugins\Hoteles\Models\DetalleReservacion', 'key' => 'reservacion_id'],

         
            $model->hasMany["payments"]= [Payment::class];

            $model->addDynamicMethod('getVigencyDate', function() {
                $settings = Settings::instance();

                $number = $settings->orders_number;
                $format = $settings->orders_code;
                $value = "";
                
                //trace_log($format."-".$number);
                $settings->orders_number." ".$settings->orders_code;

                if ($format == "h" ) {
                    //trace_log("entro en horaaa");
                    $value = $settings->orders_number." Hora";
                }elseif($format == "w"){
                    $value = $settings->orders_number." Semana";
                }elseif($format == "d"){
                    $value = $settings->orders_number." Día";
                }
            
                if ($number > 1) {
                    $value.="s";
                }
            
                return $value;
            });
           // $model->properties["vigency_time"] = $value;

            //$model->addJsonable(["vigency_time"], $value);
            //$dynamicPropertiesArray = $model->getDynamicProperties();
            //$dynamicPropertiesArray["vigency_time"] = $value;
            //$model->addDynamicProperty("properties", $dynamicPropertiesArray);
        });
    }

    /**
     * Get model class name
     * @return string
     */
    protected function getModelClass()
    {
        return Order::class;
    }

    /**
     * Get item class name
     * @return string
     */
    protected function getItemClass()
    {
        return OrderItem::class;
    }
}