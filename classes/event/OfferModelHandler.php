<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Offer;


class OfferModelHandler {

    public function subscribe(){
        Offer::extend(function($obOffer) {

            $obOffer->fillable[] = 'sold';
            
            $obOffer->addCachedField(['sold']);
            
       });
    }

}
