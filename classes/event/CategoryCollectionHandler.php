<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Classes\Collection\CategoryCollection;

class CategoryCollectionHandler {

    public function subscribe(){
        CategoryCollection::extend(function ($obCategoryList) {
            $this->addCustomMethod($obCategoryList);
        });
    }

    protected function addCustomMethod($obCategoryList)
    {
        
        $obCategoryList->addDynamicMethod('getByExternalId', function ($arrayCodes = null) use ($obCategoryList) {

            if($arrayCodes !=null ){
                $arResultIDList = (array) Category::whereIn("external_id", $arrayCodes)->lists('id');
                return $obCategoryList->intersect($arResultIDList);
            }
            
        });

        $obCategoryList->addDynamicMethod('getByCode', function ($arrayCodes = null) use ($obCategoryList) {

            if($arrayCodes !=null ){
                $arResultIDList = (array) Category::whereIn("code", $arrayCodes)->lists('id');
                return $obCategoryList->intersect($arResultIDList);
            }
            
        });
    }
}