<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Category as CategoryModel;
use Lovata\Shopaholic\Controllers\Categories as CategoriesController;
use Lovata\Shopaholic\Models\Settings;

class CategoryControllerHandler{

    public function subscribe(){

        CategoriesController::extend(function($controller) {
            // Implement behavior if not already implemented
        
            // Define property if not already defined
            $controller->addCss('https://fonts.googleapis.com/icon?family=Material+Icons');
           
           /*   
            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig');
            }
        
            // Splice in configuration safely
            $myConfigPath = '$/qchsoft/shopplus/config/category_recursive_relation.yaml';
        
            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );*/

           
        });
        
        CategoriesController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            if (!$model instanceof CategoryModel) {
               
                return;
            }
           /* pregunta si el modelo ha sido creado, si lo descomento no funciona en create 
            if (!$model->exists) {
              
                return;
            }
            */
            /*$form->addTabFields([
                'category' => [
                    'tab' => 'Parent',
                    'type'  => 'partial',
                    'path' => '$/qchsoft/shopplus/partials/_category_relation.htm',
                    'context' => 'update'
                ]
            ]);*/

            /*
            $settings = Settings::instance();
            
            if($settings->materialize_icon == true){
                $form->removeField('code');
                $form->addFields([
                    'code' => [
                        'label'   => 'Icono',
                        'type'    => 'dropdown',
                        'cssClass'=> 'custom-selectxyz'
                        
                    ]
                ]);
            }*/
            
        });
    }

    
}