<?php namespace Qchsoft\ShopPlus\Classes\Event;


use Lovata\Shopaholic\Controllers\Products as ProductController;
use Lovata\Shopaholic\Models\Product as ProductModel;
use Lovata\Shopaholic\Models\Settings;
use BackendAuth;
class ProductControllerHandler{

    public function subscribe(){

        ProductController::extend(function($controller) {
           
            // Splice in configuration safely
            $myConfigPath = '$/qchsoft/shopplus/config/product_import_extend_config.yaml';
        
            $controller->importExportConfig = $controller->mergeConfig(
                $controller->importExportConfig,
                $myConfigPath
            );

            $user = BackendAuth::getUser();

            if(!$user->is_superuser){
                
                if(!$user->hasAccess("enable-offers-tab-products")){
                    $customConfigPath = '$/qchsoft/shopplus/config/product_custom_relation_config.yaml';
                    $controller->relationConfig = $controller->mergeConfig(
                    $controller->relationConfig,
                    $customConfigPath);
                }
            }
            

        });


    }

    
}