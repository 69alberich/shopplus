<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Brand;
use Lovata\Shopaholic\Classes\Collection\BrandCollection;

class BrandCollectionHandler {

    public function subscribe(){
        BrandCollection::extend(function ($obBrandList) {
            $this->addCustomMethod($obBrandList);
        });
    }

    protected function addCustomMethod($obBrandList)
    {
        
        $obBrandList->addDynamicMethod('getByExternalId', function ($arrayCodes = null) use ($obBrandList) {

            if($arrayCodes !=null ){
                $arResultIDList = (array) Brand::whereIn("external_id", $arrayCodes)->lists('id');
                return $obBrandList->intersect($arResultIDList);
            }
            
        });
    }
}