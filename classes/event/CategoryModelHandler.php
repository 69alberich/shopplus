<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Category as CategoryModel;
class CategoryModelHandler{

    public function subscribe(){

        CategoryModel::extend(function($model) {
            /*$model->belongsTo['category'] = ['\Lovata\Shopaholic\Models\Category',
             'key' => 'parent_id'];

            $path = plugins_path("qchsoft/shopplus/assets/json/material-icons-list-json.json");
            $string = file_get_contents($path);   
            $json_file = json_decode($string, true);
            
            $icons = $json_file["categories"][0]["icons"];

            $iconsArray = array();
            for ($i=0; $i < count($icons); $i++) {
                $iconProperties =  $icons[$i];
        
                $iconsArray[$iconProperties["ligature"]] = "<i class='large material-icons'>".$iconProperties["ligature"]."</i>";
            }
            
             $model->addDynamicMethod('getCodeOptions', function() use($iconsArray) {
                return $iconsArray;
            });
            */
            $model->addDynamicMethod('scopeGetByExternalId', function($query, $codeList = null) {
                return $query->whereIn('external_id', $codeList);  
            });

            $model->addDynamicMethod('scopeGetByCode', function($query, $codeList = null) {
                return $query->whereIn('code', $codeList);  
            });
        });
        
    }

}

/*

public function ($fieldName, $value, $formData){
        
    }

*/