<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Classes\Collection\ProductCollection;

class ProductCollectionHandler {

    public function subscribe(){
        ProductCollection::extend(function ($obProductList) {
            $this->addCustomMethod($obProductList);
        });
    }

    protected function addCustomMethod($obProductList)
    {
        
        $obProductList->addDynamicMethod('getByExternalId', function ($arrayCodes = null) use ($obProductList) {
            $idsImploded = implode(',',$arrayCodes);
            
            if($arrayCodes !=null ){
                $arResultIDList = (array) Product::whereIn("external_id", $arrayCodes)
                ->orderByRaw("FIND_IN_SET(external_id,'$idsImploded')")->lists('id');
                return $obProductList->intersect($arResultIDList);
            }
            
        });
    }
}