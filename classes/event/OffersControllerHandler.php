<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Shopaholic\Models\Offer as OfferModel;
use Lovata\Shopaholic\Controllers\Offers as OffersController;

class OffersControllerHandler {

    public function subscribe(){

        OffersController::extend(function($controller) {
            
            $myConfigPath = '$/qchsoft/shopplus/config/offers_config_list.yaml';
  
            $controller->implement[] = 'Backend.Behaviors.ListController';
  
            $controller->addDynamicProperty('listConfig', $myConfigPath);


            $controller->addDynamicMethod("list", function () use ($controller){
                
                $controller->asExtension('ListController')->index();
                
                
                //
                return $controller->listRender();
            });
                
        });

        OffersController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            if (!$model instanceof OfferModel) {
               
                return;
            }
          
            $form->addTabFields([
                'sold' => [
                    'label' => 'Sold',
                    'tab'   => 'lovata.toolbox::lang.tab.settings',
                    'type'  => 'text',
                    'span'  => 'left'
                ],
            ]);
            
            
        });

    }

}
