<?php namespace Qchsoft\ShopPlus\Classes\Event;

use Lovata\Toolbox\Classes\Event\AbstractBackendFieldHandler;

use Lovata\Shopaholic\Models\Settings;


/**
 * Class ExtendSettingsFieldHandler
 * @package Lovata\OrdersShopaholic\Classes\Event\Settings
 * @author  Andrey Kharanenka, a.khoronenko@lovata.com, LOVATA Group
 */
class ExtendSettingsFieldHandler extends AbstractBackendFieldHandler
{
    /**
     * Extend form fields
     * @param \Backend\Widgets\Form $obWidget
     */
    protected function extendFields($obWidget)
    {
        $arAdditionFieldList = [
            'materialize_icon' => [
                'tab'   => 'lovata.toolbox::lang.tab.settings',
                'span' => 'left',
                'label' => 'Usar iconos materialize en categorias',
                'type' => 'switch',
                'on' => 'si',
                'off' => 'no',
            ],
            'orders_number' => [
                'tab'   => 'lovata.toolbox::lang.tab.settings',
                'label' => 'Tiempo de vigencia para las ordenes',
                'span'  => 'left',
                'type'  => 'number',
                'step' => '1',  # defaults to 'any'
                'min' => '1',   # defaults to not present
                'max' => '100',
            ],
            'orders_code' => [
                'tab'   => 'lovata.toolbox::lang.tab.settings',
                'label' => 'Formato de tiempo de vigencia',
                'span'  => 'right',
                'type'  => 'dropdown',
                'options' => [
                    'h' => "Horas",
                    'd' => "Dias",
                    'w' => "Semanas",
                ]
            ],
            
            
        ];
        $obWidget->addTabFields($arAdditionFieldList);

    }

    /**
     * Get model class name
     * @return string
     */
    protected function getModelClass() : string
    {
        return Settings::class;
    }

    /**
     * Get controller class name
     * @return string
     */
    protected function getControllerClass() : string
    {
        return \System\Controllers\Settings::class;
    }
}
