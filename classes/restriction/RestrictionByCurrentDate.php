<?php namespace QchSoft\ShopPlus\Classes\Restriction;

use Lovata\OrdersShopaholic\Interfaces\CheckRestrictionInterface;
use app;
use Lovata\OrdersShopaholic\Classes\Processor\CartProcessor;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;

class RestrictionByCurrentDate implements CheckRestrictionInterface{

    protected $cMinPrice;
    protected $cMaxPrice;
    protected $fTotalPrice;

    public function __construct($obShippingTypeItem, $arData, $arProperty, $sCode)
    {
        $this->fMinPrice = (float) array_get($arProperty, 'price_min');
        $this->fMaxPrice = (float) array_get($arProperty, 'price_max');

        $this->fTotalPrice = CartProcessor::instance()->getCartPositionTotalPriceData()->price_value;

        
    }

     /**
     * Get backend fields for restriction settings
     * @return array
     */
    public static function getFields() : array
    {
        return [
            'property[price_min]' => [
                'label'   => 'lovata.ordersshopaholic::lang.restriction.property.price_min',
                'tab'     => 'lovata.toolbox::lang.tab.settings',
                'span'    => 'left',
                'type'    => 'number',
                'context' => ['update', 'preview']
            ],
            'property[price_max]' => [
                'label'   => 'lovata.ordersshopaholic::lang.restriction.property.price_max',
                'tab'     => 'lovata.toolbox::lang.tab.settings',
                'span'    => 'right',
                'type'    => 'number',
                'context' => ['update', 'preview']
            ],
        ];
    }

    public function check() : bool
    {   
        
        $defaultCurrency = CurrencyHelper::instance()->getDefault();
        $activeCurrencyCode = CurrencyHelper::instance()->getActiveCurrencyCode();
       //echo ($defaultCurrency);
       if($activeCurrencyCode == "USD"){
        $convertePrice = $this->fTotalPrice;
       }else{
        $convertePrice = $this->fTotalPrice/$defaultCurrency->rate;
       }
        
        //echo("tengo:".$convertePrice." MIN:".$this->fMinPrice." MAX:".$this->fMaxPrice);
       //trace_log($convertePrice);
       
        $bResult = $convertePrice >= $this->fMinPrice && ($this->fMaxPrice == 0 || $convertePrice <= $this->fMaxPrice);
        
       // echo ($bResult);

        return $bResult;
    }
}