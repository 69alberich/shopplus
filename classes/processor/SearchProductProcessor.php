<?php namespace QchSoft\ShopPlus\Classes\Processor;

use DB;

class SearchProductProcessor{


    public static function search($query, $limit = null){

        $products = Db::table("lovata_shopaholic_products as products")
        ->leftJoin('lovata_shopaholic_categories as categories', 
                  'products.category_id', '=', 'categories.id')
        ->leftJoin('lovata_shopaholic_brands as brands',
                  'products.brand_id', "=", "brands.id" )
        ->select("products.name as product_name", "categories.name as category_name,", 
        "brands.name as brand_name", "products.id")
        ->whereRaw("lower(products.name) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(products.code) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(products.search_synonym) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(products.slug) LIKE  \"%$query%\" ")
        ->orWhereRaw("lower(categories.name) LIKE \"%$query%\" ")
        ->orWhereRaw("lower(brands.name) LIKE  \"%$query%\" ")
        ->when($limit, function($query, $limit){
            return $query->take($limit);
        })
        ->orderBy('products.name', 'asc')
        ->get(); 
        
        $arrayIds = array();

        $arrayIds = $products->map(function($item, $key){
            //trace_log($item->id);
            return $item->id;
            //array_push($arrayIds, ;
        });

        return $arrayIds->all();

        //trace_log($products);
    }
}


