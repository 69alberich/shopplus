<?php namespace QchSoft\ShopPlus\Classes\Processor;

use DB;

class InventoryProcessor{

    public static function checkOfferQuantity(){
        $products = Db::table("lovata_shopaholic_offers as offers")
        ->join('lovata_shopaholic_products as products',
                  'products.id', "=", "offers.product_id" )
        ->select("offers.name as name", "offers.quantity as quantity", 
        "products.preview_text as description", "products.external_id")
        ->where("offers.quantity", "<", 1)
        ->where("products.active", "=", 1)
        ->whereNull("products.deleted_at")
        ->orderBy('offers.updated_at', 'desc')
        
        ->get(); 

        return $products;
    }

    public static function checkActiveProduct(){
        $products = Db::table("lovata_shopaholic_products as products")
        ->select("products.name as name", 
        "products.preview_text as description", "products.external_id")
        ->where("products.active", "=", 0)
        ->whereNull("products.deleted_at")
        ->orderBy('products.updated_at', 'desc')
        ->get(); 

        return $products;
    }

    public static function reduceInventoryByOrder($obOrder){
        foreach ($obOrder->order_position as $order_position) {
            $arrayPosition = [
                "orden" => $obOrder->order_number,
                "Nombre" => $order_position->offer->name,
                "Tenia" => $order_position->offer->quantity,
                "Cantidad Comprada" => $order_position->quantity,
            ];
            //trace_log($order_position->offer->name." tiene: ".$order_position->offer->quantity." y resto:".$order_position->quantity);
            
            $order_position->offer->quantity -= $order_position->quantity;
            $order_position->offer->save();
            
            //trace_log("quedan:".$order_position->offer->quantity);
            $arrayPosition["Quedan"] = $order_position->offer->quantity;
    
        }
        
        $obOrder->save();
    }
    
}