<?php namespace Qchsoft\Shopplus\Classes\Import;

use \Backend\Models\ImportModel;
use Lovata\Shopaholic\Models\Offer;

class CustomOfferImport extends ImportModel{

    public $rules = [
        'external_id' => 'required',
    ];

    protected $importedData;

    public function importData($results, $sessionKey = null){
        $this->prepareImportData($results);

        foreach ($this->importedData as $row => $data) {
            try {
                if (in_array($data["external_id"], $this->arExistIDList) ) {
                  
                   $obOffer = Offer::where("external_id", $data["external_id"])->first();
                   
                   $obOffer->fill($data);

                   $obOffer->save();

                   $this->logUpdated();
                }else{
                    $this->logWarning($row, "not found");
                }

            }
            catch (\Exception $ex) {
                $this->logError($row, $ex->getMessage());
            }

        }
    }

    public function __construct(){

        $this->arExistIDList = (array) Offer::whereNotNull('external_id')->lists('external_id', 'id');
        $this->arExistIDList = array_filter($this->arExistIDList);

    }

    protected function prepareImportData($results){

        $this->importedData = $results;    


    }
}