<?php namespace Qchsoft\ShopPlus\Classes\Command;


class ArtisanList {
    const DEL_SHOPPLUS = "plugin:remove QchSoft.ShopPlus";
    const EN_SHOPPLUS = "plugin:enable QchSoft.ShopPlus";
    const DIS_SHOPPLUS = "plugin:disable QchSoft.ShopPlus";
}
