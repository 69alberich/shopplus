<?php namespace Qchsoft\ShopPlus\Models;

use Model;
use Qchsoft\ShopPlus\Models\PaymentStatus;
use Lovata\Shopaholic\Models\Currency;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\OrdersShopaholic\Models\PaymentMethod;
use Lovata\Buddies\Models\User;
/**
 * Model
 */
class Payment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_shopplus_payments';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $filliable = [
        "order_id",
        "mount",
        "reference",
        "payment_method_id",
        "currency_id",
        "payment_status_id",
        "usable_id",
        "usable_type",
    ];

    public $belongsTo = [
        'status'         => [PaymentStatus::class, "key" => "payment_status_id"],
        'currency'       => [Currency::class],
        'order'       => [Order::class],
        'payment_method' => [PaymentMethod::class],
        'user' => [User::class, "key"=> "usable_id"]
    ];

    public $attachOne = [
        'file'=> 'System\Models\File'
      ];

    
      public function scopeGetByUser($query, $id){
          return $query->where("usable_id", $id);
      }

      public function scopeGetByOrderId($query, $id){
        return $query->where("order_id", $id);
    }

      /**
     * Get currency_symbol attribute value
     * @return null|string
     */
    protected function getCurrencySymbolAttribute()
    {
        //Get currency object
        $obCurrency = $this->currency;
        if (empty($obCurrency)) {
            return null;
        }

        return $obCurrency->symbol;
    }

    public function getUsableTypeOptions($value, $formData){
        return ['Lovata\Buddies\Models\User' => 'General'];
    }

    /*public function getUserOptions($value, $formData){
        $data = post();
        trace_log($data);
        if ($value == 'status') {
           // return ['all' => 'All', ...];
        }
        else {
           // return ['' => '-- none --'];
        }
    }*/
}
