<?php namespace QchSoft\ShopPlus\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'shop_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
